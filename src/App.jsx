import basketImg from "./assets/basket.png";
import { useState, useEffect } from "react";

function App() {
  const [inputValue, setInputValue] = useState("");
  const [groceryItems, setGroceryItems] = useState([
    {
      name: "Banana",
      quantity: 1,
      completed: true,
    },
    {
      name: "Bread",
      quantity: 2,
      completed: false,
    },
  ]);
  const [isCompleted, setIsCompleted] = useState(false);

  useEffect(() => {
    determindeCompletedStatus();
  });

  const handleChangeInputValue = (e) => {
    setInputValue(e.target.value);
  };

  const determindeCompletedStatus = () => {
    if (!groceryItems.length) {
      return setIsCompleted(false);
    }

    let isAllCompleted = true;

    groceryItems.forEach((item) => {
      if (!item.completed) isAllCompleted = false;
    });

    setIsCompleted(isAllCompleted);
  };

  const handleAddGroceryItem = (e) => {
    if (e.key === "Enter") {
      if (inputValue) {
        const updatedGroceryList = [...groceryItems];

        const itemIndex = updatedGroceryList.findIndex(
          (item) => item.name === inputValue
        );

        if (itemIndex === -1) {
          updatedGroceryList.push({
            name: inputValue,
            quantity: 1,
            completed: false,
          });
        } else {
          updatedGroceryList[itemIndex].quantity++;
        }

        setGroceryItems(updatedGroceryList);
        setInputValue("");
      }
    }
  };

  const handleRemoveItem = (name) => {
    setGroceryItems([...groceryItems].filter((item) => item.name !== name));
  };

  const handleUpdateCompleteStatus = (status, index) => {
    const updatedGroceryList = [...groceryItems];
    updatedGroceryList[index].completed = status;
    setGroceryItems(updatedGroceryList);
  };

  const renderGroceryList = () => {
    return groceryItems.map((item, index) => (
      <li
        key={item.name}
        className="flex flex-row items-center justify-between ring-1 ring-slate-600 p-3 rounded my-4"
      >
        <div className="flex flex-row items-center gap-2">
          <input
            type="checkbox"
            className="relative peer shrink-0 w-4 h-4 border-2 border-blue-500 rounded-sm bg-white checked:bg-blue-800 checked:border-0"
            onChange={(e) => {
              handleUpdateCompleteStatus(e.target.checked, index);
            }}
            value={item.completed}
            checked={item.completed}
          />
          <label className="text-xl font-bold text-slate-600 ">
            {item.name}{" "}
            {item.quantity > 1 ? <span>x{item.quantity}</span> : null}
          </label>
        </div>
        <div>
          <button onClick={() => handleRemoveItem(item.name)}>X</button>
        </div>
      </li>
    ));
  };

  return (
    <main className="App">
      <div className="h-screen flex flex-col items-center justify-center gap-3">
        {isCompleted ? (
          <h4 className="text-xl text-teal-600 font-bold">You have DONE!</h4>
        ) : null}

        <div className="flex flex-col items-center gap-2">
          <h1 className="text-3xl font-black text-slate-600">Shopping List</h1>
          <img className="w-40 h-40" src={basketImg} alt="" />
          <input
            className="ring-2 ring-slate-600 p-2 outline-none rounded-md"
            type="text"
            placeholder="Add an item"
            onChange={handleChangeInputValue}
            onKeyDown={handleAddGroceryItem}
            value={inputValue}
          />
        </div>
        <div className="w-80 py-4">
          <ul>{renderGroceryList()}</ul>
        </div>
        <button
          className="bg-amber-500 px-2 rounded-md font-medium"
          onClick={() => {
            const updatedGroceryList = [...groceryItems].map((item) => {
              return {
                ...item,
                completed: false,
              };
            });
            setGroceryItems(updatedGroceryList);
          }}
        >
          Clear All Checked
        </button>
      </div>
    </main>
  );
}

export default App;
